/* objetos en javascript */

automovil ={
    marca:"OTRA",
    modelo:"Yaris",
    descripcion: "Toyota Yaris 2024 Automatico Aire Acondicionado Precio 330,000",
    imagen : "/img/carro4.png"
}
// Mostrar el Objeto
console.log("Automovil " + automovil);

console.log( "Marca :"+ automovil.marca );
console.log( "Modelo :" + automovil.modelo);
console.log(" Descripcion" + automovil.descripcion);
console.log("Imagen " + automovil.imagen);

const modelo = document.getElementById("modelo");
modelo.innerHTML= automovil.modelo;

const marco = document.getElementById("marca");
marca.innerHTML = automovil.marca;

/*  Arreglo de Objetos */

automoviles=[{ 
marca:"TOYOTA",
modelo:"Yaris",
descripcion: "Toyota Yaris 2024 Automatico Aire Acondicionado Precio 330,000",
imagen : "/img/carro4.png"
},

{
    marca:"NISSAN",
    modelo:"centra",
    descripcion: "Nissan Centra 2024 Automatico Aire Acondicionado Precio 330,000",
    imagen : "/img/carro5.png"

},{
    marca:"FORD",
    modelo:"Bronco",
    descripcion: "Ford Bronco 2024 Automatico Aire Acondicionado Precio 330,000",
    imagen : "/img/carro6.png"
}];
// mostrar todos los Objetos
for(let con=0; con< automoviles.length; ++con){

    console.log( "Marca :"+ automoviles[con].marca );
    console.log( "Modelo :" + automoviles[con].modelo);
    console.log(" Descripcion" + automoviles[con].descripcion);
    console.log("Imagen " + automoviles[con].imagen);
    

}

const selMarc = document.getElementById('selMarc');
selMarc.addEventListener('change',function  cambio(){
 let opcion = selMarc.value;
 alert(opcion);

});